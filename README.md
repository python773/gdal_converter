Program is converting input rasters into determined format.
Input formats: *.bil, *.tif, *.tiff
Output formats: *.tif, *.tiff

Raster can be converter with options:
- background color change
- epsg change
- tfw creation
- tiled GeoTIFF files
- BigTiff files
- Overviews add
- Compression add
- Change resolution
- Add alpha band
- and also cut to buffer/cut to buffer with tile - resampling and cutting method need to select


