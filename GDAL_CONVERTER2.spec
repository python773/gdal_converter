from kivy_deps import sdl2, glew
# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['GDAL_CONVERTER.py'],
             pathex=[],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
a.datas += [('GDAL_CONVERTER.kv','d:\\MSZ\\_Repos\\GDAL_Converter\\GDAL_CONVERTER.kv','DATA')]
a.datas += [('modules\Converter.py','d:\\MSZ\\_Repos\\GDAL_Converter\\modules\\Converter.py','DATA')]
a.datas += [('ico\\GDAL_Converter.ico','d:\\MSZ\\_Repos\\GDAL_Converter\\ico\\GDAL_Converter.ico','DATA')]

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,  
		  *[Tree(p) for p in (sdl2.dep_bins)],
          [],
          name='GDAL_CONVERTER',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
		  icon='d:\\MSZ\\_Repos\\GDAL_Converter\\ico\\GDAL_Converter.ico',
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None )
