import os
os.environ['PROJ_LIB'] = 'GDAL_venv\Lib\site-packages\osgeo\data\proj'
os.environ['GDAL_DATA'] ='GDAL_venv\Lib\site-packages\osgeo\data\gdal'
from osgeo import gdal
import pandas as pd
from shapely.geometry import box
from modules.functions import get_files
from modules.convertion_ABC import ConvertionABC


class ConvertionTile(ConvertionABC):

    def bil2TIFF_tile(file, parameters):
        
        temp = ConvertionTile.createTemp(file)
        input_format, out_format, overviews, kwargs = ConvertionTile.parameters_translator(parameters)
        tile_cut,tile_list = ConvertionTile.raster_tile_intersection(file,parameters['buffor_SHP'],parameters['tile_SHP'],parameters['tile_column'],parameters['src_EPSG'])
 
        if not os.path.isfile(tile_cut):
            return tile_cut
        else:
            try:  
                #Save file
                for tile in tile_list:
                    new_file = os.path.join(temp,os.path.basename(file))                      #redirect to temp
                    new_file = new_file.replace(input_format,"") + "###" + tile + out_format  #rename file
                    tile_column=parameters['tile_column']
                    query = f"{tile_column}='{tile}'"
                    img_out = gdal.Warp(new_file, file, cutlineDSName = tile_cut,cropToCutline = True,cutlineWhere=query, **kwargs)

                #Set overviews
                img_out.BuildOverviews(overviewlist=overviews)

            except Exception as e:
                return f"Can't process file {new_file} due to error: '" + str(e) + "'"

    def merge_tiles(files_parameters):
        
        temp = ConvertionTile.find_temp(files_parameters)

        df = pd.DataFrame(get_files(temp,'.tif')[1]+get_files(temp,'.tiff')[1], columns=['Files']) #create DataFrame from files
        df['Tile_name'] = df['Files'].str.split("###").str[1].replace(to_replace=r'\..*', value='', regex=True)                  #Get tile column
        tiles = df['Tile_name'].unique().tolist()
        vrt_list=list()
        
        #Save tile VRT from input TIFFs
        for tile in tiles:
            filter = df.loc[df['Tile_name']==tile,['Files']]['Files'].tolist()  
            vrt = os.path.join(temp,tile + ".vrt")
            vrt_list.append(vrt)
            gdal.BuildVRT(vrt,filter)
        return vrt_list


    def save_results(vrt, parameters):
        input_format, out_format, overviews, kwargs = ConvertionTile.parameters_translator(parameters)
        
        try:
            new_file=vrt.replace('_temp\\','').replace('.vrt',out_format)
            
            #Save file
            img_out = gdal.Warp(new_file,vrt , **kwargs)

            #Set overviews
            img_out.BuildOverviews(overviewlist=overviews)
            
        except Exception as e:
            return f"Can't process file {new_file} due to error: '" + str(e) + "'"






#ConvertionTile.bil2TIFF_tile(r'D:/MSZ/test/HSM1\\L133S1_003.bil','.bil','black','black','3006','3006','.tif',True, True, True, '2,4,8,16,32','LZW',False, 'CenterPoint',None,None,None,tile,'CELL_NAME')
#ConvertionTile.bil2TIFF_tile(r'D:/MSZ/test/HSM1\\L133S1_004.bil','.bil','black','black','3006','3006','.tif',True, True, True, '2,4,8,16,32','LZW',False, 'CenterPoint',None,None,None,tile,'CELL_NAME')
#ConvertionTile.save_results(ConvertionTile.merge_tiles())


vrt_list=['d:\\MSZ\\test\\HSM1\\_temp\\0001154246.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001155266.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001156286.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001157306.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001158326.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001159346.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001160366.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001161386.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001162406.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001163426.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001164446.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001165466.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001166486.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001167506.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001168526.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001169546.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001170566.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001171586.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001172606.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001173626.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001174646.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001175666.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001176686.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001177705.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001177706.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001178725.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001179745.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001180765.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001181785.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001182805.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001183825.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001184845.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001185865.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001186885.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001187905.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001188925.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001189945.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001191984.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001193004.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001194024.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001195044.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001196064.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001197084.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001198104.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001199124.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001200144.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001201164.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001201165.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001202185.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001203205.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001204225.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001205245.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001206265.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001207285.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001208305.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001209325.vrt', 'd:\\MSZ\\test\\HSM1\\_temp\\0001210345.vrt']




