import os
from modules.functions import *
os.environ['PROJ_LIB'] = resource_path('GDAL_venv\Lib\site-packages\osgeo\data\proj')
os.environ['GDAL_DATA'] = resource_path('GDAL_venv\Lib\site-packages\osgeo\data\gdal')
from osgeo import osr
import rasterio as rio
import geopandas as gpd
from shapely.geometry import box
import shutil, glob
from abc import ABC


class ConvertionABC(ABC):
    temp=''

    @staticmethod
    def find_temp(files_parameters):
        unique_path=list()
        [unique_path.append(os.path.dirname(_[0])+"\\_temp") for _ in files_parameters if os.path.dirname(_[0]+"\\_temp") not in unique_path]
        return unique_path[0]

    @staticmethod
    def createTemp(file_path):
        try:
            directory = os.path.dirname(file_path)
            temp=os.path.join(directory,'_temp')
            if not os.path.exists(temp):
                os.makedirs(temp)
            return temp
        except Exception as e:
            return f"Can't create temp folder from {file_path} due to error {e}"

    @staticmethod
    def epsg_converter(EPSG):
        CRS = osr.SpatialReference()
        CRS.ImportFromEPSG(int(EPSG))
        return str(CRS)
    
    @staticmethod
    def prj_creator(input_file, EPSG):
        import re
        CRS = str(ConvertionABC.epsg_converter(EPSG))
        CRS = ''.join(CRS.split())
        prj_file = re.sub('\.\w{3}$','.prj',input_file)
        with open(prj_file, 'w') as w:
            w.write(CRS)

    @staticmethod
    def copy_shp_to_temp(file_name, temp, shp_path): #also added file_name
        for file in glob.glob(shp_path.replace('.shp','.*')): #get all SHP files -dbf, cpg etc.
            tile_f_name = os.path.splitext(os.path.basename(file))[0]
            tile_ext = os.path.splitext(os.path.basename(file))[1]
            tile_copy = os.path.join(temp, file_name+"_"+tile_f_name+tile_ext)
            if tile_ext=='.shp' : tile_copy_shp = tile_copy
            shutil.copy(file, tile_copy)
        return tile_copy_shp 

    @staticmethod
    def raster_buffor_intersection(raster_path, buffer_path, CRS):   
        try:
            #prepare temp and raster name
            temp = ConvertionABC.createTemp(raster_path)
            file_name = os.path.splitext(os.path.basename(raster_path))[0]
            output_shp = os.path.join(temp,file_name+'_range.shp')
            
            buffor_copy_shp = ConvertionABC.copy_shp_to_temp(file_name,temp,buffer_path)

            #Get raster boundary box
            raster = rio.open(raster_path)
            bounds = raster.bounds
            df_raster = gpd.GeoDataFrame({"id":1,"name":file_name,"geometry":[box(*bounds)]}).set_crs(f'epsg:{CRS}', allow_override=True)

            #Read buffor SHP
            buffor = gpd.read_file(buffor_copy_shp).set_crs(f'epsg:{CRS}', allow_override=True)

            #Clip data
            raster_buffor_intersection = gpd.clip(df_raster, buffor)
            raster_buffor_intersection.set_crs(f'epsg:{CRS}', allow_override=True)
            raster_buffor_intersection.to_file(output_shp)
            ConvertionABC.prj_creator(output_shp, CRS)
            return output_shp

        except Exception as e:
            return f"Buffor has no overlap with raster {file_name}"


    @staticmethod
    def raster_tile_intersection(raster_path,buffor_path, tile_path, tile_column, CRS):
        try:
            #prepare temp and raster name
            temp = ConvertionABC.createTemp(raster_path)
            file_name = os.path.splitext(os.path.basename(raster_path))[0]
            output_shp = os.path.join(temp,file_name+'_tile_cut.shp')
            
            if buffor_path and tile_path: #intersect buffor and tile
                buffor_copy_shp = gpd.read_file(ConvertionABC.copy_shp_to_temp(file_name, temp, buffor_path)).set_crs(f'epsg:{CRS}', allow_override=True)
                tile_copy_shp = gpd.read_file(ConvertionABC.copy_shp_to_temp(file_name, temp, tile_path)).set_crs(f'epsg:{CRS}', allow_override=True)
                intersect = gpd.overlay(buffor_copy_shp, tile_copy_shp, how='intersection')
            elif tile_path:
                intersect = gpd.read_file(ConvertionABC.copy_shp_to_temp(file_name, temp, tile_path)).set_crs(f'epsg:{CRS}', allow_override=True)
            elif buffor_path:
                intersect = gpd.read_file(ConvertionABC.copy_shp_to_temp(file_name, temp, buffor_path)).set_crs(f'epsg:{CRS}', allow_override=True)
                
        
            #Get raster boundary box
            raster = rio.open(raster_path)
            bounds = raster.bounds
            df_raster = gpd.GeoDataFrame({"id":1,"name":file_name,"geometry":[box(*bounds)]}).set_crs(f'epsg:{CRS}', allow_override=True)
            #Clip raster bound to buffor_tile_intersect
            overlayer = gpd.overlay(df_raster, intersect, how='intersection')
            
            if buffor_path and not tile_path:
                overlayer.to_file(output_shp)
                return output_shp
            else:
                tile_list =  overlayer[tile_column]
                overlayer.to_file(output_shp)
                return output_shp, tile_list
        
        except Exception as e:
            return f"Tile has no overlap with raster {file_name}  {e}"


    @staticmethod
    def parameters_translator(parameters):

        #translation dictionary
        kwargs={'multithread':True} 
        

        #Convert parameters to GDAL
        kwargs.update({'srcNodata':255}) if parameters['src_bg_color']=='white' else (kwargs.update({'srcNodata':0}) if parameters['src_bg_color']=='black' else '')
        kwargs.update({'dstNodata':255}) if parameters['dst_bg_color']=='white' else (kwargs.update({'dstNodata':0}) if parameters['src_bg_color']=='black' else '')
        
        if parameters['src_EPSG'] : kwargs.update({'srcSRS':ConvertionABC.epsg_converter(parameters['src_EPSG'])}) 
        if parameters['dst_EPSG'] : kwargs.update({'dstSRS':ConvertionABC.epsg_converter(parameters['dst_EPSG'])}) 
        

        tfw ="TFW=YES" if parameters['tfw'] == True else ""
        tiled ="TILED=YES" if parameters['tiled'] == True else ""
        bigtiff ="BIGTIFF=YES" if parameters['bigtiff'] == True else ""

        if parameters['compression']=='LZW': compression, quality="COMPRESS=LZW",''
        elif parameters['compression']=='DEFLATE': compression, quality="COMPRESS=DEFLATE",''
        elif parameters['compression']=='JPEG Q2': compression, quality="COMPRESS=JPEG","JPEG_QUALITY=75"
        elif parameters['compression']=='JPEG Q3': compression, quality="COMPRESS=JPEG","JPEG_QUALITY=50"
        elif parameters['compression']=='JPEG Q4': compression, quality="COMPRESS=JPEG","JPEG_QUALITY=25"
        elif parameters['compression']=='None': compression, quality=''
        
        if parameters['addalpha']: kwargs.update({'dstAlpha':"255 255 255"}) if parameters['dst_bg_color']=='white' else  kwargs.update({'dstAlpha':"0 0 0"})
        color_interpret="PHOTOMETRIC=RGB"
        overviews=[int(n) for n in parameters['overviews'].split(',')]

        if parameters['resolution']:
            kwargs.update({'xRes':parameters['resolution']})
            kwargs.update({'yRes':parameters['resolution']})
            kwargs.update({'resampleAlg':parameters['resampling']})
        
        if parameters['buffor_SHP'] or parameters['tile_SHP']: 
            if parameters['cutting_method']!='CenterPoint': kwargs.update({'cutlineBlend':0.55})

        #Build option list with non empty values
        tiffOptions=list(filter(None,[tfw, tiled, bigtiff, compression,quality, color_interpret]))
        kwargs.update({'creationOptions':tiffOptions}) 

        return parameters['input_format'], parameters['out_format'], overviews, kwargs

