import os
os.environ['PROJ_LIB'] = 'GDAL_venv\Lib\site-packages\osgeo\data\proj'
os.environ['GDAL_DATA'] ='GDAL_venv\Lib\site-packages\osgeo\data\gdal'
from osgeo import gdal
from modules.convertion_ABC import ConvertionABC

class ConvertionBuffor(ConvertionABC):
    
    @staticmethod
    def bil2TIFF_buffor(file, parameters):
    
        input_format, out_format, overviews, kwargs = ConvertionBuffor.parameters_translator(parameters)
        bufferCut = ConvertionBuffor.raster_tile_intersection(file,parameters['buffor_SHP'],parameters['tile_SHP'],parameters['tile_column'],parameters['src_EPSG'])

        if not os.path.isfile(bufferCut):
            return bufferCut

        else:
            try:
                new_file=file.replace(input_format,out_format)
                #Save file
                img_out = gdal.Warp(new_file,file, cutlineDSName = bufferCut,cropToCutline = True, **kwargs)

                #Set overviews
                img_out.BuildOverviews(overviewlist=overviews)
                
            except Exception as e:
                return f"Can't process file {new_file} due to error: '" + str(e) + "'"
