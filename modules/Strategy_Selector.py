from modules.convertion import *
from modules.convertion_buffor import *
from modules.convertion_tile import *

class StrategySelector:
    def __init__(self, files_parameters):
        self.files_parameters = files_parameters

    def __str__(self):
        return self.select_strategy().__name__
        
    def check_buffor(self):
        if self.files_parameters[0][1]['buffor_SHP']: return True
                
    def check_tile(self):
        if self.files_parameters[0][1]['tile_SHP']: return True

    def select_strategy(self):
        check_buffor = self.check_buffor()
        check_tile = self.check_tile()

        if check_tile: 
            return ConvertionTile.bil2TIFF_tile
        elif check_buffor:
            return ConvertionBuffor.bil2TIFF_buffor
        else:
            return Convertion.bil2TIFF_GDAL
    


