from osgeo import gdal
from modules.convertion_ABC import ConvertionABC

class Convertion(ConvertionABC):
    
    @staticmethod
    def bil2TIFF_GDAL(file, parameters):
        input_format, out_format, overviews, kwargs = Convertion.parameters_translator(parameters)

        try:
            new_file=file.replace(input_format,out_format)
            
            #Save file
            img_out = gdal.Warp(new_file,file, **kwargs)

            #Set overviews
            img_out.BuildOverviews(overviewlist=overviews)
            
        except Exception as e:
            return f"Can't process file {new_file} due to error: '" + str(e) + "'"

#os.environ['PROJ_LIB'] = r'GDAL_venv\Lib\site-packages\pyproj\proj_dir\share\proj'
#Convertion_TIFF.bil2TIFF_GDAL(r'D:/MSZ/test/HSM1\\L133S1_022.bil','.bil','black','black','3006','3006','.tif',True, True, True, '2,4,8,16,32','LZW',False, 'near',1,None,None,None)