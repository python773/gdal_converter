# -*- coding: utf-8 -*-
import os, sys, shutil
os.environ['PROJ_LIB'] = 'GDAL_venv\Lib\site-packages\osgeo\data\proj'
os.environ['GDAL_DATA'] ='GDAL_venv\Lib\site-packages\osgeo\data\gdal'
from osgeo import gdal
from osgeo import osr
from datetime import datetime
import geopandas as gpd
from difflib import SequenceMatcher


def remove_temp(directory):
    temp_folders=list()
    [temp_folders.append(x[0]) for x in os.walk(directory) if  '_temp' in x[0]]
    try:
        for path in temp_folders:
            if os.path.exists(path):
                shutil.rmtree(path)
    except Exception as e:
        return f"Can't remove temp folder {path} due to error {e}"


def shp_first_column(shp_path):
    if shp_path:
        shapes = gpd.read_file(shp_path)
        columns=list(shapes.columns.values)
        columns.remove('geometry')
        return '' if len(columns)==0 else columns[0]
    else:
        return ''

def correct_column_name(column_name, shp_path):
    if shp_path:
        columns=list(gpd.read_file(shp_path).columns.values)
        if column_name in columns:
            return column_name
        else:
            max_match=0
            max_match_column=''
            for columnSHP in reversed(columns):
                ratio = SequenceMatcher(None, column_name, columnSHP).ratio()
                if ratio >= max_match:
                    max_match=ratio
                    max_match_column=columnSHP
            return max_match_column
    else:
        return ''

# General functions
def mylogger(message):
    now = datetime.now()
    with open(f'{now.strftime("%Y-%m-%d__%H_%M_%S")}__log.txt','a') as f:
        for line in message:
            f.write(now.strftime("%Y-%m-%d %H:%M:%S")+'\t' + line + '\n')


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


def permission_checker(path):
    try:
        os.mkdir(os.path.join(path, 'test'))
        os.rmdir(os.path.join(path, 'test'))
        return ""
    except PermissionError:
        return "[color=#ff0000][b]You don't have permission to write files in this folder[/b][/color]"


def get_files(path, extension):
    file_list=[]
    if not path:
        path = os.getcwd()
    try:
        for root, dirs, files in os.walk(path):
            for name in files:
                add = os.path.join(root, name)
                filename, file_extension = os.path.splitext(name)
                if not "$RECYCLE" in str(add) and file_extension==extension:
                    file_list.append(add)  # file list
    except:
        return "Error while getting file list."
    
    return f"[b]Converting {len(file_list)}{extension} files...[/b]", file_list


def files_parameters(file_list,parameters):
    return [(file,) + (parameters,) for i,file in enumerate(file_list)]


def bil2TIFF_old(file, input_format, src_bg_color, dst_bg_color,src_EPSG,dst_EPSG,out_format, tfw, tiled, bigtiff,overviews, compression,addalpha):

    new_file=file.replace(input_format,"")+out_format
    img = gdal.Open(file)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(int(dst_EPSG))
    driver = gdal.GetDriverByName("GTiff")

    #Convert parameters to GDAL
    src_bg_color=255 if src_bg_color=='white' else 0
    dst_bg_color=255 if dst_bg_color=='white' else 0
    tfw ="TFW=YES" if tfw == True else ""
    tiled ="TILED=YES" if tiled == True else ""
    bigtiff ="BIGTIFF=YES" if bigtiff == True else ""
    overviews=[int(n) for n in overviews.split(',')]
    if compression=='LZW': compression, quality="COMPRESS=LZW",''
    elif compression=='DEFLATE': compression, quality="COMPRESS=DEFLATE",''
    elif compression=='JPEG Q2': compression, quality="COMPRESS=JPEG","JPEG_QUALITY=75"
    elif compression=='JPEG Q3': compression, quality="COMPRESS=JPEG","JPEG_QUALITY=50"
    elif compression=='JPEG Q4': compression, quality="COMPRESS=JPEG","JPEG_QUALITY=25"
    elif compression=='None': compression, quality='',
    color_interpret="PHOTOMETRIC=RGB"
    addalpha='ALPHA=PREMULTIPLIED'

    
    #Build option list with non empty values
    tiffOptions=list(filter(None,[tfw, tiled, bigtiff, compression,quality, color_interpret,addalpha]))
    #tiffOptions=["TILED=YES","COMPRESS=LZW","TFW=YES","BIGTIFF=YES","PHOTOMETRIC=RGB", "ALPHA=YES"]
       
    #Create TIFF with options
    img_out=driver.CreateCopy(new_file, img, options=tiffOptions)

    #Set nodata_value
    img_out.GetRasterBand(1)
    img_out.GetRasterBand(1).SetNoDataValue(dst_bg_color)

    #Set reprojection
    img_out.SetProjection(srs.ExportToWkt())

    #Set overviews
    img_out.BuildOverviews(overviewlist=overviews)


#os.environ['PROJ_LIB'] = r'GDAL_venv\Lib\site-packages\pyproj\proj_dir\share\proj'
#bil2TIFF(r'D:/MSZ/test/HSM1\\L133S1_023.bil', '.bil', 'black', 'black', '3006', '.tiff', True, True, True, '2,4,8,16,32', 'LZW')


