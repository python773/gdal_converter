import os, shutil

def remove_temp(directory):
    temp_folders=list()
    [temp_folders.append(x[0]) for x in os.walk(directory) if  '_temp' in x[0]]
    try:
        for path in temp_folders:
            if os.path.exists(path):
                shutil.rmtree(path)
    except Exception as e:
        return f"Can't remove temp folder {path} due to error {e}"
    

directory=r'd:\MSZ\test\HSM1'
print(remove_temp(directory))