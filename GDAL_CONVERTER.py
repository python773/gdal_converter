import os, time
import multiprocessing as mp
from modules.functions import *
from modules.convertion import *
from modules.convertion_buffor import *
from modules.convertion_tile import *
from modules.strategy_selector import StrategySelector

os.environ['PROJ_LIB'] = resource_path('GDAL_venv\Lib\site-packages\osgeo\data\proj')
os.environ['GDAL_DATA'] = resource_path('GDAL_venv\Lib\site-packages\osgeo\data\gdal')


if __name__ == '__main__':
    mp.freeze_support()
    from kivy.app import App
    from kivy.uix.screenmanager import ScreenManager, Screen
    from kivy.core.window import Window
    import tkinter as tk
    from tkinter import filedialog 
    import threading

    #main window settings
    Window.size = (700, 350)
    Window.clearcolor = (1, 1, 1, 1)


    class GDAL_CONVERTER(App):
        def build(self):
            sm = ScreenManager()
            sm.add_widget(MainScreen(name='MainScreen'))
            sm.add_widget(SettingsScreen(name='SettingsScreen'))
            self.icon = resource_path("ico/GDAL_Converter.ico")
            return sm
    
    class SettingsScreen(Screen):
        pass
  
    class MainScreen(Screen):
        pathVar = ''  #variable to store path dir
        pathBufforVar=''
        pathTileVar=''
        
        def __init__(self, **kwargs):
            super(MainScreen, self).__init__(**kwargs)

        def get_input_path(self):
            self.clean_message_button()  # clean window
            root = tk.Tk()
            root.iconbitmap(resource_path("ico/GDAL_Converter.ico"))
            root.withdraw()
            pathVar = filedialog.askdirectory()
            self.ids.InputPath.text = pathVar  # Change text of InputPath
            root.destroy()
            # if permission denied - not run
            check = permission_checker(pathVar)
            if check:
                self.message(check)
                self.ids.Start.disabled = True

        def get_buffor_path(self):
            root = tk.Tk()
            root.iconbitmap(resource_path("ico/GDAL_Converter.ico"))
            root.withdraw()
            pathBufforVar = filedialog.askopenfilename(filetypes=[("SHP files", ".shp")])
            self.ids.InputPathBuffor.text = pathBufforVar  # Change text of InputPath
            root.destroy()

        def get_tile_path(self):
            root = tk.Tk()
            root.iconbitmap(resource_path("ico/GDAL_Converter.ico"))
            root.withdraw()
            pathTileVar = filedialog.askopenfilename(filetypes=[("SHP files", ".shp")])
            self.ids.InputPathTile.text = pathTileVar  # Change text of InputPath
            root.destroy()      
                
        def clean_message_button(self):
            self.message("")
            self.ids.Start.disabled = False

        def start_mp(self):
            if not self.ids.InputPath.text:
                self.message('[color=#ff0000][b]Please select Path[/b][/color]')
            elif not self.manager.get_screen("SettingsScreen").ids.SrcEPSG.text:
                self.message('[color=#ff0000][b]Please insert Source EPSG code[/b][/color]')
            elif not self.manager.get_screen("SettingsScreen").ids.DestEPSG.text:
                self.message('[color=#ff0000][b]Please insert Destination EPSG code[/b][/color]')
            elif self.ids.InputPathTile.text and not self.ids.Tile_column.text:
                self.message('[color=#ff0000][b]Please add Tile Column name[/b][/color]')
            else:
                self.message('[b]Working...[/b]')
                threading.Thread(target=self.process_parallel).start()

        def process_parallel(self):
            #measure time
            start = time.time()
            report,file_list=get_files(self.ids.InputPath.text,self.manager.get_screen("SettingsScreen").ids.InputFormat.text)
            self.message(report)

            #parallel work
            parameters=self.get_settings()
            pool = mp.Pool(mp.cpu_count()-2)
            strategy = StrategySelector(files_parameters(file_list,parameters))
            res = pool.starmap(strategy.select_strategy(), strategy.files_parameters)
            pool.close()

            if "tile" in strategy.__str__():
                ConvertionTile.merge_tiles(files_parameters(file_list,parameters))
                report,vrt_list=get_files(self.ids.InputPath.text,'.vrt')
                pool = mp.Pool(mp.cpu_count()-2)
                res = pool.starmap(ConvertionTile.save_results, files_parameters(vrt_list,parameters))
                pool.close()

            #Remove temp if checkbox is active
            if self.manager.get_screen("SettingsScreen").ids.RemoveTemp.active:
                remove_temp(self.ids.InputPath.text)
            #get errors
            errors = list(filter(None,res))
            if errors: mylogger(errors)
            duration=round(time.time()-start)  
            self.message(f'[b]Process done in {duration}s with {len(errors)} errors[/b]')


        def message(self, message): # message box - show message
            self.ids.Message.text = message

        def get_settings(self):
            settings=dict()  #dictionary with all parameters
            settings['input_format'] = self.manager.get_screen("SettingsScreen").ids.InputFormat.text
            settings['src_bg_color'] = self.manager.get_screen("SettingsScreen").ids.SourceBG.text
            settings['dst_bg_color'] = self.manager.get_screen("SettingsScreen").ids.DestBG.text
            settings['src_EPSG'] = self.manager.get_screen("SettingsScreen").ids.SrcEPSG.text           
            settings['dst_EPSG'] = self.manager.get_screen("SettingsScreen").ids.DestEPSG.text
            settings['out_format'] = self.manager.get_screen("SettingsScreen").ids.OutputFormat.text
            settings['tfw'] = self.manager.get_screen("SettingsScreen").ids.TFW_creation.active
            settings['tiled'] = self.manager.get_screen("SettingsScreen").ids.Tiled.active
            settings['bigtiff'] = self.manager.get_screen("SettingsScreen").ids.BigTiff.active
            settings['overviews'] = self.manager.get_screen("SettingsScreen").ids.Overviews.text    
            settings['compression'] = self.manager.get_screen("SettingsScreen").ids.Compression.text
            settings['addalpha']=self.manager.get_screen("SettingsScreen").ids.AlfaBand.active
            settings['cutting_method'] = self.manager.get_screen("SettingsScreen").ids.Cutting_Method.text  
            settings['resampling'] = self.manager.get_screen("SettingsScreen").ids.Resampling_method.text  
            settings['resolution'] = self.manager.get_screen("SettingsScreen").ids.Resolution.text 
            settings['buffor_SHP'] = self.ids.InputPathBuffor.text  
            settings['tile_SHP'] = self.ids.InputPathTile.text  
            settings['tile_column'] =  correct_column_name(self.ids.Tile_column.text,self.ids.InputPathTile.text)  #correct tile_column if not correct
            return settings
             
    GDAL_CONVERTER().run()
    
