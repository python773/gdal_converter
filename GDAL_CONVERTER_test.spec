from kivy_deps import sdl2, glew
# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['GDAL_CONVERTER.py'],
             pathex=[],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
a.datas += [('GDAL_CONVERTER.kv','d:\\MSZ\\_Repos\\GDAL_Converter\\GDAL_CONVERTER.kv','DATA')]
a.datas += [('modules\\convertion_ABC.py','d:\\MSZ\\_Repos\\GDAL_Converter\\modules\\convertion_ABC.py','DATA')]
a.datas += [('modules\\convertion_buffor.py','d:\\MSZ\\_Repos\\GDAL_Converter\\modules\\convertion_buffor.py','DATA')]
a.datas += [('modules\\convertion_tile.py','d:\\MSZ\\_Repos\\GDAL_Converter\\modules\\convertion_tile.py','DATA')]
a.datas += [('modules\\convertion.py','d:\\MSZ\\_Repos\\GDAL_Converter\\modules\\convertion.py','DATA')]
a.datas += [('modules\\functions.py','d:\\MSZ\\_Repos\\GDAL_Converter\\modules\\functions.py','DATA')]
a.datas += [('modules\\strategy_selector.p','d:\\MSZ\\_Repos\\GDAL_Converter\\modules\\strategy_selector.py','DATA')]
a.datas += [('ico\\GDAL_Converter.ico','d:\\MSZ\\_Repos\\GDAL_Converter\\ico\\GDAL_Converter.ico','DATA')]
a.datas += Tree('GDAL_venv\\Lib\\site-packages\\osgeo\\data\\proj', prefix='GDAL_venv\\Lib\\site-packages\\osgeo\\data\\proj')
a.datas += Tree('GDAL_venv\\Lib\\site-packages\\osgeo\\data\\gdal', prefix='GDAL_venv\\Lib\\site-packages\\osgeo\\data\\gdal')
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,  
		  *[Tree(p) for p in (sdl2.dep_bins)],
          [],
          name='GDAL_CONVERTER',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
		  icon='d:\\MSZ\\_Repos\\GDAL_Converter\\ico\\GDAL_Converter.ico',
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None )
